package numberPlateRecognition;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

public class ShowImage extends JFrame {

    public ShowImage(Mat mat) {
        String imgName = "tmp.jpg";
        Highgui.imwrite(imgName, mat);
        JFrame frame = new JFrame("Image");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setLocationRelativeTo(null);

        ImageIcon image = new ImageIcon(imgName);
        frame.setSize(image.getIconWidth() + 10, image.getIconHeight() + 35);

        JLabel label1 = new JLabel("", image, JLabel.CENTER);
        frame.getContentPane().add(label1);

        frame.validate();
        frame.setVisible(true);
    }
}
