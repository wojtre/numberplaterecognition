package numberPlateRecognition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

public class NumberPlate {
    private Mat plateImageInMat;
    private List<Sign> separatedSigns = new ArrayList<Sign>();
    private Projection projectionOnX = new Projection();
    private Projection projectionOnY = new Projection();
    private List<Integer> placesOfPartition = new ArrayList<Integer>();

    public NumberPlate(Mat mat) {
        plateImageInMat = mat;
    }

    public NumberPlate(String ImageAdress) {
        plateImageInMat = Highgui.imread(ImageAdress,
                Highgui.CV_LOAD_IMAGE_GRAYSCALE);
    }

    public void separateSigns() {
        prepareImage();
        projectionOnX.projectOnAxisX(plateImageInMat);
        placesOfPartition = projectionOnX.findPlacesOfPartition(0.86);
        projectionOnY.projectOnAxisY(plateImageInMat);
        new Charts()
                .printProjectionXFromArray(projectionOnX.projectionOnAxiscopy);
        new Charts().printProjectionYFromArray(projectionOnY.projectionOnAxis);
        Collections.sort(placesOfPartition);
        Iterator<Integer> it = placesOfPartition.iterator();
        Integer left = 0, right = 0;
        while (it.hasNext()) {
            right = it.next();
            if ((int) (right - left) > 3) {
                separatedSigns.add(new Sign(cutArea(left, right, 0,
                        plateImageInMat.rows())));
            }
            left = right;
        }
        printPlacesOfPartition();
        showSeparatedSigns();
        for (int i = 0; i < separatedSigns.size(); i++) {
            separatedSigns.get(i).truncateSign();
        }
        Iterator<Sign> iter = separatedSigns.iterator();
        while (iter.hasNext()) {
            if (!iter.next().isValid(plateImageInMat.size())) {
                iter.remove();
            }
        }
        showSeparatedSigns();
    }

    private Mat cutArea(int left, int right, int up, int down) {
        if (left >= right) {
            return null;
        }
        Mat newImage = new Mat(down - up, right - left, plateImageInMat.type());
        plateImageInMat.submat(up, down, left, right).copyTo(newImage);
        return newImage;
    }

    private void prepareImage() {
        Imgproc.adaptiveThreshold(plateImageInMat, plateImageInMat, 255,
                Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY,
                1 + (int) (plateImageInMat.cols() / 16.0) * 2, 1);
        showPlate();
        // Imgproc.dilate(plateImageInMat, plateImageInMat, new Mat());
        // Imgproc.erode(plateImageInMat, plateImageInMat, new Mat());

    }

    public void showPlate() {
        ImageUtils.displayImage(ImageUtils.Mat2BufferedImage(plateImageInMat));
    }

    public void printPlacesOfPartition() {
        Collections.sort(placesOfPartition);
        System.out.println(placesOfPartition);
    }

    public void showSeparatedSigns() {
        for (Sign sign : separatedSigns) {
            sign.show();
        }
    }
}

class Projection {
    public int[] projectionOnAxis;
    public int[] projectionOnAxiscopy;
    public int maxValue = 0;
    public int indexOfMaxValue = 0;
    public int averageValue = 0;
    private final double boundConst = 0.7;

    public List<Integer> findPlacesOfPartition(final double peakConst) {
        List<Integer> placesOfPartition = new ArrayList<Integer>();

        findMaxValueAndItsIndex();
        calculateAverageValue();
        while (true) {
            int tmpIndexOfMax = returnsIndexOfMax();
            int leftBound = findLeftBoundIndex(tmpIndexOfMax);
            int rightBound = findRightBoundIndex(tmpIndexOfMax);
            if (projectionOnAxis[tmpIndexOfMax] < (int) (maxValue * peakConst)) {
                return placesOfPartition;
            }
            zeroizeIntervalOnProjection(leftBound, rightBound);
            placesOfPartition.add(new Integer(tmpIndexOfMax));
        }
    }

    public List<Integer> findPlacesOfPartitionY(final double peakConst) {
        List<Integer> placesOfPartition = new ArrayList<Integer>();

        findMaxValueAndItsIndex();
        calculateAverageValue();
        while (placesOfPartition.size() < 2) {
            int tmpIndexOfMax = returnsIndexOfMax();
            int leftBound = findLeftBoundIndex(tmpIndexOfMax);
            int rightBound = findRightBoundIndex(tmpIndexOfMax);
            if (projectionOnAxis[tmpIndexOfMax] < (int) (maxValue * peakConst)) {
                return placesOfPartition;
            }
            zeroizeIntervalOnProjection(leftBound, rightBound);
            placesOfPartition.add(new Integer(tmpIndexOfMax));
        }
        return placesOfPartition;
    }

    public void projectOnAxisX(Mat mat) {
        projectionOnAxis = new int[mat.cols()];
        projectionOnAxiscopy = new int[mat.cols()];
        for (int i = 0; i < mat.cols(); i++) {
            for (int j = 0; j < mat.rows(); j++) {
                if (mat.get(j, i)[0] > 0) {
                    projectionOnAxis[i]++;
                    projectionOnAxiscopy[i]++;
                }
            }
        }
    }

    public void projectOnAxisY(Mat mat) {
        projectionOnAxis = new int[mat.cols()];
        for (int i = 0; i < mat.rows(); i++) {
            for (int j = 0; j < mat.cols(); j++) {
                if (mat.get(i, j)[0] > 0) {
                    projectionOnAxis[j]++;
                }
            }
        }
    }

    public void calculateAverageValue() {
        int sum = 0;
        for (int i : projectionOnAxis) {
            sum += i;
        }
        averageValue = (int) (sum / (double) projectionOnAxis.length);
    }

    public void findMaxValueAndItsIndex() {
        int max = 0;
        int index = 0;
        for (int i = 0; i < projectionOnAxis.length; i++) {
            if (max < projectionOnAxis[i]) {
                max = projectionOnAxis[i];
                index = i;
            }
        }
        maxValue = max;
        indexOfMaxValue = index;
    }

    public int returnsIndexOfMax() {
        int max = 0;
        int index = 0;
        for (int i = 0; i < projectionOnAxis.length; i++) {
            if (max < projectionOnAxis[i]) {
                max = projectionOnAxis[i];
                index = i;
            }
        }
        return index;
    }

    public int findLeftBoundIndex(int indexOfMax) {
        for (int i = indexOfMax; i >= 0; i--) {
            if (projectionOnAxis[i] < (int) (projectionOnAxis[indexOfMax] * boundConst)) {
                return i;
            }
        }
        return 0;
    }

    public int findRightBoundIndex(int indexOfMax) {
        for (int i = indexOfMax; i < projectionOnAxis.length; i++) {
            if (projectionOnAxis[i] < (int) (projectionOnAxis[indexOfMax] * boundConst)) {
                return i;
            }
        }
        return projectionOnAxis.length - 1;
    }

    public void zeroizeIntervalOnProjection(int left, int right) {
        for (int i = left; i <= right; i++) {
            projectionOnAxis[i] = 0;
        }
    }

}