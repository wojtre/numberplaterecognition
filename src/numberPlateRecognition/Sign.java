/**
 * 
 */
package numberPlateRecognition;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * @author Wojciech Trefon
 *
 */
public class Sign {
    private Mat signImageInMat;
    private String recognizedSign;
    private List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

    public Sign(Mat m) {
        signImageInMat = m;
    }

    public Mat returnSignInMat() {
        return signImageInMat;
    }

    public boolean isValid(Size plateSize) {
        if (signImageInMat.rows() < plateSize.height / 2) {
            return false;
        }
        return true;
    }

    public void truncateSign() {
        Mat invertedSign = returnSignWithInvertedColor();
        findImageContours(invertedSign);
        int indexOfContour = findSignContourIndex();
        leaveOnlyContourArea(indexOfContour);
    }

    private void leaveOnlyContourArea(int indexOfContour) {
        Rect rect = Imgproc.boundingRect(contours.get(indexOfContour));
        Mat cont = new Mat(signImageInMat.size(), signImageInMat.type(),
                new Scalar(0));
        Imgproc.drawContours(cont, contours, indexOfContour, new Scalar(255),
                -1);
        Core.subtract(cont, signImageInMat, signImageInMat);
        signImageInMat = returnSignWithInvertedColor();
        signImageInMat.submat(rect).copyTo(signImageInMat);
    }

    private int findSignContourIndex() {
        double area = 0;
        int index = 0;
        Rect contourRect;
        for (int i = 0; i < contours.size(); i++) {
            contourRect = Imgproc.boundingRect(contours.get(i));
            if (area < contourRect.height * contourRect.width) {
                area = contourRect.height * contourRect.width;
                index = i;
            }
        }
        return index;
    }

    private void findImageContours(Mat copyMat) {
        Imgproc.findContours(copyMat, contours, new Mat(), Imgproc.RETR_CCOMP,
                Imgproc.CHAIN_APPROX_NONE);
    }

    private Mat returnSignWithInvertedColor() {
        Mat invert = new Mat(signImageInMat.rows(), signImageInMat.cols(),
                signImageInMat.type(), new Scalar(255));
        Mat copyMat = new Mat(signImageInMat.rows(), signImageInMat.cols(),
                signImageInMat.type());
        Core.subtract(invert, signImageInMat, copyMat);
        return copyMat;
    }

    public void show() {
        if (signImageInMat != null) {
            ImageUtils.displayImage(ImageUtils
                    .Mat2BufferedImage(signImageInMat));
        }
        else {
            System.err.println("No sign to display");
        }
    }
}
